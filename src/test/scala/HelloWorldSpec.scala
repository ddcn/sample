import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.must.Matchers.convertToAnyMustWrapper

class HelloWorldSpec extends AnyFreeSpec {
  
  "Hello world" - {

    "add must add numbers" in {
      HelloWorld.add(1, 2) mustEqual 3
    }

  }

}
